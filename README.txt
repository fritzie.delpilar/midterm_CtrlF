CSCI40-F

Del Pilar, Fritzie Dianne, B., 211983
Salto, Mary Adelaide, A., 215208
Tan, Lance Cedric B., 204954

Midterm Proj: Widget v2

App Assignments: 
Del Pilar - Announcementboard.v2
Salto - Dashboard.v2
Tan   - Forum.v2

Date of Submission: May 16, 2023

We, members of CtrlF, truthfully completed this project with all our efforts and knowledge.

References:
DateTimeField - https://www.geeksforgeeks.org/datetimefield-django-forms/
"pre-receive hook declined" error - https://timmousk.com/blog/git-pre-receive-hook-declined/#why-the-pre-receive-hook-declined-error-happens


Members' Signature:
(sgd) Del Pilar, Fritzie Dianne B., May 15, 2023
(sgd) Salto, Mary Adelaide A., May 15, 2023