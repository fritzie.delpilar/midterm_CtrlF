from django.db import models
from django.utils import timezone
from dashboard.models import WidgetUser
from django.urls import reverse


class Announcement(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField(max_length=10000)
    author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE)
    pub_datetime = models.DateTimeField(default=timezone.now, null=True, blank=True)

    def date_format(self):
        return '{}' .format(self.pub_datetime.strftime("%m/%d/%Y"))
    
    def time_format(self):
        return '{}' .format(self.pub_datetime.strftime("%H:%M %p"))
    
    def getLike(self):
        try:
            return '{}'.format(Reaction.objects.get(name="Like", announcement=self).tally)
        except:
            return '0'
    
    def getLove(self):
        try:
            return '{}'.format(Reaction.objects.get(name="Love", announcement=self).tally)
        except:
            return '0'
    
    def getAngry(self):
        try:
            return '{}'.format(Reaction.objects.get(name="Angry", announcement=self).tally)
        except:
            return '0'

    def get_absolute_url(self):
        return reverse(
            'announcement_board:announcement-details',
            kwargs={'pk': self.pk},
        )

    def __str__(self):
        return '{} by {} {} published {}, {}: {}'.format(
            self.title,
            self.author.first_name,
            self.author.last_name,
            self.pub_datetime.strftime("%m/%d/%Y"),
            self.pub_datetime.strftime("%H:%M %p"),
            self.body,
        )
    
class Reaction(models.Model):
    Like = 'Like'
    Love = 'Love'
    Angry = 'Angry'
    REACTIONS = ((Like, 'Like'), 
        (Love, 'Love'), 
        (Angry, 'Angry'))
    

    name = models.CharField(max_length=10, choices=REACTIONS, default=Like)
    tally = models.IntegerField(default = 0)
    announcement = models.ForeignKey(Announcement, on_delete = models.CASCADE)

    def __str__(self):
        return '{} reactions for {}' .format(self.name, self.announcement)