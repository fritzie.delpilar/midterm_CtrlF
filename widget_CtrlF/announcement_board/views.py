from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from .models import Announcement

def index(request):
    announcement_view = "Widget's Announcement Board <br/>"
    announcements = Announcement.objects.all()
    return render(
        request, 
        'announcements/announcements.html', 
        {'announcements_list': announcements},
    )


class AnnouncementsDetailView(DetailView):
    model = Announcement
    template_name = 'announcements/announcement-details.html'
    

class AnnouncementsCreateView(CreateView):
    model = Announcement
    fields = 'title', 'body', 'author'
    template_name = 'announcements/announcement-add.html'
    

class AnnouncementsUpdateView(UpdateView):
    model = Announcement
    fields = 'title', 'body', 'author'
    template_name = 'announcements/announcement-edit.html'


    # for a in announcements:
    #     like_tally=0
    #     love_tally=0
    #     angry_tally=0
    #     for r in reactions:
    #         if r.name=="Like" and r.announcement==a:
    #             like_tally+=r.tally
    #         elif r.name=="Love" and r.announcement==a:
    #             love_tally+=r.tally
    #         elif r.name=="Angry" and r.announcement==a:
    #             angry_tally+=r.tally
    #     announcement_view +=  "<br/> {} by {} {} published {}: <br/> {} <br/> Like: {} <br/> Love: {} <br/> Angry: {} <br/>".\
    #         format(a.title,
    #         a.author.first_name,
    #         a.author.last_name,
    #         a.pub_datetime.strftime('%m/%d/%Y, %H:%M %p'),
    #         a.body,
    #         like_tally,
    #         love_tally,
    #         angry_tally)
        
    # return HttpResponse(announcement_view)