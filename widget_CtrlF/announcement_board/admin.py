from django.contrib import admin

from .models import Announcement, Reaction

class AnnouncementAdmin(admin.ModelAdmin):
    model = Announcement
    list_display = ('title', 'author', 'pub_datetime')
    search_fields = ('title', 'author',)
    list_filter = ['author', 'pub_datetime']

class ReactionAdmin(admin.ModelAdmin):
    model = Reaction
    list_display = ('name', 'tally', 'announcement')
    search_fields = ('name',)
    list_filter = ['name']

admin.site.register(Announcement, AnnouncementAdmin)
admin.site.register(Reaction, ReactionAdmin)