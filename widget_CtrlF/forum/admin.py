from django.contrib import admin

from .models import ForumPost, Reply


class ForumPostAdmin(admin.ModelAdmin):
    model = ForumPost

    search_fields = ('title', 'body', 'author',)

    list_display = ('title', 'body', 'author', 'pub_datetime',)

    list_filter = ('title', 'author', 'pub_datetime',)


class ReplyAdmin(admin.ModelAdmin):
    model = Reply

    search_fields = ('body', 'author',)

    list_display = ('body', 'author', 'pub_datetime',)

    list_filter = ('author', 'pub_datetime')


admin.site.register(ForumPost, ForumPostAdmin)
admin.site.register(Reply, ReplyAdmin)