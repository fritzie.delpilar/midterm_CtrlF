from django.shortcuts import render
from django.http import HttpResponse

from .models import ForumPost, Reply
# Create your views here.
def index(request):
    page_content = "<p>Widget's Forum</p> Forum Posts:<br>"
    
    for post in ForumPost.objects.all():
        page_content += '{} by {} {} posted {}:<br>{}<br>'.format(
            post.title, post.author.first_name, post.author.last_name, 
            post.pub_datetime.strftime('%m/%d/%Y, %H:%M %p'), post.body
        )
        for reply in Reply.objects.all():
            if reply.forum_post.title==post.title:
                page_content += 'Reply by {} {} posted {}:<br>{}<br>'.format(
                    reply.author.first_name, reply.author.last_name, 
                    reply.pub_datetime.strftime('%m/%d/%Y, %H:%M %p'),
                    reply.body
                )
    
    return HttpResponse(page_content)