from django.db import models
from django.utils import timezone
from dashboard.models import WidgetUser


class ForumPost(models.Model):
    title = models.CharField(max_length=100)
    body = models.TextField()
    author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE)
    pub_datetime =  models.DateTimeField()

    def __str__(self):
        return self.title
    

class Reply(models.Model):
    forum_post = models.ForeignKey(ForumPost, on_delete=models.CASCADE, null=True)
    body = models.TextField()
    author = models.ForeignKey(WidgetUser, on_delete=models.CASCADE) 
    pub_datetime = models.DateTimeField(default=timezone.now, null=True, blank=True) 

    def __str__(self):
        return 'Reply by {} {} posted {}'.format(
            self.author.first_name, self.author.last_name, 
            self.pub_datetime.strftime('%m/%d/%Y, %H:%M %p')
            )